const express= require('express');
const config = require('config');
const mongoose = require('mongoose');
const debug = require('debug')
const startupDebug = require("debug")("app:startup")
const dbDebug = require('debug')('app:db');
const debugError = debug('error');
const {logger} = require("./middlewares/logger")
const province = require("./controllers/province")
const district = require("./controllers/district")
const app = express();
app.use(express.json())

app.use( logger,province );
app.use( logger,district );

mongoose.connect(config.get("database"),{
    useUnifiedTopology:true,
    useNewUrlParser:true
})
  .then(()=>{
      dbDebug("DB connceted");
  })
  .catch(err=>{
      debugError("Failed to connect due to ",err);
  })
if(!config.get("database")){
    debugError("Failed to connect to Database")
    process.exit(-1);
}
if(!config.get("PORT")){
    debugError("Invalid port!")
    process.exit(-1);
}
var port =process.env.PORT||config.get("PORT");
app.listen(port,()=> startupDebug(`Listening on port ${port}..`))