const express = require('express')
const router = express.Router()
const { Province } = require("../models/province")
const { District,validate } = require('../models/district')

router.get("/districts",async(req,res)=>{
    try{
        const districts = await District.find()
        res.send(districts)
    }
    catch(ex){
        res.status(500).send(ex.message)
    }
})

router.get("/district/:id",async(req,res)=>{
    const district = await District.findById(req.params.id)
    if(!district) return res.status(404).send("District not availabe")
    res.send(district)
})

router.post("/new/district",async(req,res)=>{
    if(!req.body.provinceId) return res.status(400).send("province Id is required!")
    const province = await Province.findById(req.body.provinceId)
    if(!province) return res.status(404).send("Province not available")
    const {error} = validate(req.body)
    if(error) return res.status(400).send(error.details[0].message)
    const newDistrict = new District({
        provinceInfo:{
            id:province._id,
            Name: province.provinceName
        },
        districtName:req.body.districtName,
        Mayor:req.body.Mayor
    })
    try{
        await newDistrict.save()
        res.send(newDistrict)
    }
    catch(ex){
        res.status(400).send(ex.message)
    }
})

router.put("/edit/district/:id",async(req,res)=>{
   const district = await District.findById(req.params.id)
   if(!district) return res.status(400).send("District not available")
   const {error} = validate(req.body)
   if(error) return res.status(400).send(error.details[0].message)
   try{
       const updatedDistrict = await District.findByIdAndUpdate(req.params.id,{
            districtName:req.body.districtName,
            Mayor:req.body.Mayor
       },{new:true})
       res.send(updatedDistrict)
   }
   catch(ex){
       res.status(400).send(ex.message)
   }
})

router.delete("/delete/district/:id",async(req,res)=>{
    const district = await District.findById(req.params.id)
    if(!district) return res.status(400).send("District not available")
    try{
        const deletedDistrict = await District.findByIdAndDelete(req.params.id)
        res.send(deletedDistrict)
    }
    catch(ex){
        res.status(400).send(ex.message)
    }
})
module.exports = router;