const express= require('express')
const router = express.Router()
const { Province,validate } = require("../models/province")


router.get("/provinces",async(req,res)=>{
    try{
        const provinces = await Province.find()
        res.send(provinces)
    }
    catch(ex){
        res.status(500).send(ex.message)
    }
})

router.get("/province/:id",async(req,res)=>{
    try{
        const province = await Province.findById(req.params.id)
        if(!province) return res.status(404).send("Province not availbe")
        res.send(province)
    }
    catch(ex){
        res.status(500).send(ex.message)
    }
})

router.post("/new/province",async(req,res)=>{
    const {error} = validate(req.body)
    if(error) return res.status(400).send(error.details[0].message)
    const newProvince = new Province({
        provinceName:req.body.provinceName,
        provinceSize:req.body.provinceSize
    })
    try{
        await newProvince.save()
        res.send(newProvince)
    }
    catch(ex){
        res.status(400).send(ex.message)
    }
})

router.put("/edit/province/:id",async(req,res)=>{
    const{error} = validate(req.body)
    if(error) return res.status(400).send(error.details[0].message)
    const province = await Province.findById(req.params.id)
    if(!province) return res.status(404).send("Province not availabe")
    try{
        const updatedProvince = await Province.findByIdAndUpdate(req.params.id,{
        provinceName:req.body.provinceName,
        provinceSize:req.body.provinceSize
        },{new:true})
       res.send(updatedProvince)
    }
    catch(ex){
        res.status(500).send(ex.message)
    }
})

router.delete("/delete/province/:id",async(req,res)=>{
    const province = await Province.findById(req.params.id)
    if(!province) return res.status(404).send("Province not available")
    try{
        const deletedProvince = await Province.findByIdAndDelete(req.params.id)
        res.send(deletedProvince)
    }
    catch(ex){
        res.status(500).send(ex.message)
    }
})
module.exports=router;