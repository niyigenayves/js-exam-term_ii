
const apiDebug = require("debug")("app:apis")
function logger(req,res,next){
    const method = req.method
    const url = req.originalUrl
    apiDebug(`API Endpoint: "${method}" http method with url:"${url}"`);
    next();
}
module.exports.logger=logger;